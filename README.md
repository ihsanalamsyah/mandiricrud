# Mandiri - CRUD

Ini adalah repository CRUD. Ikuti pentunjuk dibawah untuk menjalankan repository

## Prasyaratan
Sebelum memulai menyentuh repository, pastikan project teman-teman memenuhi prasyarat berikut:
- sudah install System.Net.Http
- sudah install System.Data.SqlClient
- sudah install Swashbuckle.AspNetCore
- sudah install MSSQL
- sudah install git

## CRUD
CRUD adalah sebuah manipulasi database dengan beberapa method create data, read data, update data, dan delete data


## Bahasa pemograman
Pada repository ini menggunakan bahasa pemograman C# Core MVC

### Run
Untuk menjalankan aplikasi ini, jalankan visual studio

