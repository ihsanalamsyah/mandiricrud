﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Metrics;

namespace WebApplication3.Controllers
{

    public class ProductUserNameController : ControllerBase
    {

        public string API_MESSAGE_ERROR = "Error system please contact IT service desk";
        public string connectionString = "server=(LocalDb)\\MSSQLLocalDB;database=CRUD_Mandiri;integrated Security=SSPI;";

      
        [HttpPost, Route("SaveProductUserName")]
        public ResultDynamic SaveUserNameDetail([BindRequired] int productId, [BindRequired] int userId, string CreatedBy)
        {

            int result = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    INSERT INTO dbo.ProductUserName (ProductId, UserId, CreatedDate, CreatedBy, RowStatus)
                    SELECT @0, @1, GETDATE(), @2, 0
                    ";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", productId);
                        _cmd.Parameters.AddWithValue("@1", userId);
                        _cmd.Parameters.AddWithValue("@2", CreatedBy);
                        result = _cmd.ExecuteNonQuery();

                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done save" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data already exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }

        [HttpPost, Route("DeleteProductUserName")]
        public ResultDynamic DeleteProductUserName([BindRequired] int productId, [BindRequired] int userId, string CreatedBy)
        {

            int result = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    UPDATE dbo.ProductUserName
                    SET RowStatus = 1, UpdatedBy = @2, UpdatedDate = GETDATE()
                    Where UserId = @1 AND ProductId = @0";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", productId);
                        _cmd.Parameters.AddWithValue("@1", userId);
                        _cmd.Parameters.AddWithValue("@2", CreatedBy);
                        result = _cmd.ExecuteNonQuery();

                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done save" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data already exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }
        [HttpGet, Route("GetProductUserName")]
        public ResultDynamic GetProductUserName()
        {
            var productUserName = new List<ProductUserName>();
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";SELECT p.ProductName, DP.ProductDescription, DP.Price, UM.UserName, DUN.FullName FROM dbo.ProductUserName PUN
                    INNER JOIN dbo.Product p ON p.Id = PUN.ProductId AND p.RowStatus = 0
					LEFT JOIN dbo.DetailProduct DP ON DP.ProductId = p.Id AND DP.RowStatus = 0
                    INNER JOIN dbo.UserName UM ON UM.Id = PUN.UserId AND UM.RowStatus = 0
					LEFT JOIN dbo.DetailUserName DUN ON DUN.UserId = UM.Id AND DUN.RowStatus = 0
                    WHERE PUN.RowStatus = 0 ORDER BY p.ProductName ASC";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        SqlDataReader rdr = _cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            var productUserName2 = new ProductUserName();
                            productUserName2.ProductName = rdr["ProductName"].ToString();
                            productUserName2.ProductDescription = rdr["ProductDescription"].ToString();
                            productUserName2.Price = string.IsNullOrEmpty(rdr["Price"].ToString()) ? 0 : int.Parse(rdr["Price"].ToString());
                            productUserName2.UserName = rdr["UserName"].ToString();
                            productUserName2.FullName = rdr["FullName"].ToString();

                            productUserName.Add(productUserName2);
                        }

                    }
                    _con.Close();
                }

                if (productUserName != null)
                    return new ResultDynamic { status = true, data = productUserName };
                else
                    return new ResultDynamic { status = false, message = "Data is Empty" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }
    }
}
