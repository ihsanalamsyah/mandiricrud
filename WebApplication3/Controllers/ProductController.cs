using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Metrics;

namespace WebApplication3.Controllers
{
   
    public class ProductController : ControllerBase
    {

        public string API_MESSAGE_ERROR = "Error system please contact IT service desk";
        public string connectionString = "server=(LocalDb)\\MSSQLLocalDB;database=CRUD_Mandiri;integrated Security=SSPI;";

     

        [HttpGet, Route("GetProduct")]
        public ResultDynamic GetProduct()
        {
            var product = new List<Product>();
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @"SELECT p.ProductName, DP.ProductDescription, DP.Quantity, DP.Price FROM dbo.Product p
                    LEFT JOIN dbo.DetailProduct DP ON DP.ProductId = p.Id AND DP.RowStatus = 0
                    WHERE RowStatus = 0 ORDER BY ProductName ASC";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        SqlDataReader rdr = _cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            var product2 = new Product();
                            product2.Id = int.Parse(rdr["Id"].ToString());
                            product2.ProductName = rdr["ProductName"].ToString();
                            product2.DescriptionProduct = rdr["ProductDescription"].ToString();
                            product2.UserId = int.Parse(rdr["UserId"].ToString());
                            product2.CreatedDate = DateTime.Parse(rdr["CreatedDate"].ToString());
                            product2.CreatedBy = rdr["CreatedBy"].ToString();
                            product2.UpdatedDate = string.IsNullOrEmpty(rdr["UpdatedDate"].ToString()) ? null : DateTime.Parse(rdr["UpdateDate"].ToString());
                            product2.UpdatedBy = string.IsNullOrEmpty(rdr["UpdatedDate"].ToString()) ? null : rdr["UpdatedBy"].ToString();

                            product.Add(product2);
                        }

                    }
                    _con.Close();
                }

                if (product != null)
                    return new ResultDynamic { status = true, data = product };
                else
                    return new ResultDynamic { status = false, message = "Data is Empty" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }                 
        }

        [HttpPost, Route("SaveProduct")]
        public ResultDynamic SaveProduct([BindRequired] string productName, [BindRequired] string productDescription, [BindRequired] int userId, string CreatedBy)
        {

            int result = 0;
            int productId = 0;
            int result2 = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    INSERT INTO dbo.Product (ProductName, ProductDescription, UserId, CreatedDate, CreatedBy, RowStatus)
                    SELECT @0, @1, @2, GETDATE(), @3, 0
                    Where NOT EXISTS( SELECT 1 FROM dbo.Product p WHERE p.ProductName = @0)";

                    string queryStatement2 = @";
                    Select Id From dbo.Product Where ProductName = @0
                    ";

                    string queryStatement3 = @";
                    INSERT INTO dbo.ProductUserName (ProductId, UserId, CreatedDate, CreatedBy, RowStatus)
                    VALUES(@0, @1, GETDATE(), @2, 0)
                    ";
                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", productName);
                        _cmd.Parameters.AddWithValue("@1", productDescription);
                        _cmd.Parameters.AddWithValue("@2", userId);
                        _cmd.Parameters.AddWithValue("@3", CreatedBy);
                        result = _cmd.ExecuteNonQuery();
                        _con.Close();
                        if (result > 0)
                        {
                            using (SqlCommand _cmd2 = new SqlCommand(queryStatement2, _con))
                            {
                                _con.Open();
                                _cmd2.Parameters.AddWithValue("@0", productName);
                                SqlDataReader rdr = _cmd2.ExecuteReader();

                                while (rdr.Read())
                                {
                                    productId = int.Parse(rdr["Id"].ToString());         
                                }
                                _con.Close();
                                if (productId > 0)
                                {
                                    using (SqlCommand _cmd3 = new SqlCommand(queryStatement3, _con))
                                    {
                                        _con.Open();
                                        _cmd3.Parameters.AddWithValue("@0", productId);
                                        _cmd3.Parameters.AddWithValue("@1", userId);
                                        _cmd3.Parameters.AddWithValue("@2", CreatedBy);
                                        result2 = _cmd3.ExecuteNonQuery();
                                    }
                                    _con.Close();
                                }
                            }
                        }
                        else
                            return new ResultDynamic { status = false, data = result, message = "Data already exists" };
                    }

                }
                if (result2 > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done save & mapping" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data failed mapping" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }                  
        }

        [HttpPost, Route("DeleteProduct")]
        public ResultDynamic DeleteProduct([BindRequired] string productName, string CreatedBy)
        {

            int result = 0;
            int productId = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    UPDATE INTO dbo.Product
                    SET RowStatus = 1, UpdatedBy = @1, UpdatedDate = GETDATE()
                    Where ProductName = @0";

                    string queryStatement2 = @";
                    SELECT top 1 Id FROM dbo.Product
                    Where ProductName = @0";

                    string queryStatement3 = @";
                    UPDATE INTO dbo.DetailProduct
                    SET RowStatus = 1, UpdatedBy = @1, UpdatedDate = GETDATE()
                    Where ProductId = @0";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", productName);
                        _cmd.Parameters.AddWithValue("@1", CreatedBy);
                        result = _cmd.ExecuteNonQuery();                                           
                    }
                    _con.Close();
                    _con.Open();
                    using (SqlCommand _cmd2 = new SqlCommand(queryStatement2, _con))
                    {
                       
                        _cmd2.Parameters.AddWithValue("@0", productName);
                        SqlDataReader rdr = _cmd2.ExecuteReader();

                        while (rdr.Read())
                        {
                            productId = int.Parse(rdr["Id"].ToString());
                        }                                          
                    }
                    _con.Close();
                    _con.Open();
                    using (SqlCommand _cmd3 = new SqlCommand(queryStatement3, _con))
                    {
                        _cmd3.Parameters.AddWithValue("@0", productId);
                        _cmd3.Parameters.AddWithValue("@1", CreatedBy);
                        result = _cmd3.ExecuteNonQuery();                       
                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done delete" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data not exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }

        [HttpGet, Route("SearchProductBy")]
        public ResultDynamic SearchProductBy([BindRequired] string searchBy, [BindRequired] string param)
        {
            var product = new List<Product>();
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    var parama = string.Concat("%", param, "%");
                    string queryStatement = "SELECT * FROM dbo.Product WHERE RowStatus = 0 ";
                    if (searchBy == "ProductDescription")
                        queryStatement += "AND ProductDescription Like @0 ORDER BY ProductName ASC";
                    else
                        queryStatement += "AND ProductName Like @0 ORDER BY ProductName ASC";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", parama);
                        SqlDataReader rdr = _cmd.ExecuteReader();
                        
                        while (rdr.Read())
                        {
                            var product2 = new Product();
                            product2.Id = int.Parse(rdr["Id"].ToString());
                            product2.ProductName = rdr["ProductName"].ToString();
                            product2.DescriptionProduct = rdr["ProductDescription"].ToString();
                            product2.UserId = int.Parse(rdr["UserId"].ToString());
                            product2.CreatedDate = DateTime.Parse(rdr["CreatedDate"].ToString());
                            product2.CreatedBy = rdr["CreatedBy"].ToString();
                            product2.UpdatedDate = string.IsNullOrEmpty(rdr["UpdatedDate"].ToString()) ? null : DateTime.Parse(rdr["UpdateDate"].ToString());
                            product2.UpdatedBy = string.IsNullOrEmpty(rdr["UpdatedDate"].ToString()) ? null : rdr["UpdatedBy"].ToString();

                            product.Add(product2);
                        }

                    }
                    _con.Close();
                }

                if (product != null)
                    return new ResultDynamic { status = true, data = product };
                else
                    return new ResultDynamic { status = false, message = "Data is Empty" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }

        [HttpPost, Route("UpdateProduct")]
        public ResultDynamic UpdateProduct([BindRequired] string productName, [BindRequired] string productDescription, [BindRequired] int CreatedBy)
        {

            int result = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    UPDATE dbo.Product
                    SET ProductDescription = @1, UpdatedBy = @2, UpdatedDate = GETDATE()
                    Where ProductName = @0";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", productName);
                        _cmd.Parameters.AddWithValue("@1", productDescription);
                        _cmd.Parameters.AddWithValue("@2", CreatedBy);
                        result = _cmd.ExecuteNonQuery();

                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done update" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data already exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }

        [HttpPost, Route("SaveProductDetail")]
        public ResultDynamic SaveProductDetail([BindRequired] int productId, [BindRequired] string productDescription, int quantity, int price,  string CreatedBy)
        {

            int result = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    INSERT INTO dbo.DetailProduct (ProductId, ProductDescription, Quantity, Price, CreatedDate, CreatedBy, RowStatus)
                    SELECT @0, @1, @2, @3, GETDATE(), @4, 0
                    Where NOT EXISTS( SELECT 1 FROM dbo.DetailProduct DP WHERE DP.ProductId = @0)";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", productId);
                        _cmd.Parameters.AddWithValue("@1", productDescription);
                        _cmd.Parameters.AddWithValue("@2", quantity);
                        _cmd.Parameters.AddWithValue("@3", price);
                        _cmd.Parameters.AddWithValue("@4", CreatedBy);
                        result = _cmd.ExecuteNonQuery();

                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done save" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data already exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }

        [HttpPost, Route("UpdateProductDetail")]
        public ResultDynamic UpdateProductDetail([BindRequired] int productId, [BindRequired] string productDescription, int quantity, int price, string CreatedBy)
        {

            int result = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    UPDATE dbo.DetailProduct
                    SET ProductDescription = @1,Quantity = @2, Price = @3,  UpdatedBy = @4, UpdatedDate = GETDATE()
                    Where UserId = @0";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", productId);
                        _cmd.Parameters.AddWithValue("@1", productDescription);
                        _cmd.Parameters.AddWithValue("@2", quantity);
                        _cmd.Parameters.AddWithValue("@3", price);
                        _cmd.Parameters.AddWithValue("@4", CreatedBy);
                        result = _cmd.ExecuteNonQuery();

                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done save" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data already exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }
    }
}
