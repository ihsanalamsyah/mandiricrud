﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Metrics;

namespace WebApplication3.Controllers
{

    public class UserNameController : ControllerBase
    {

        public string API_MESSAGE_ERROR = "Error system please contact IT service desk";
        public string connectionString = "server=(LocalDb)\\MSSQLLocalDB;database=CRUD_Mandiri;integrated Security=SSPI;";

        [HttpPost, Route("Registration")]
        public ResultDynamic Registration([BindRequired] string userName, [BindRequired] string email, [BindRequired] string password, string CreatedBy)
        {

            int result = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    INSERT INTO dbo.UserName (UserName, Email, [Password], CreatedDate, CreatedBy, RowStatus)
                    SELECT @0, @1, @2, GETDATE(), @3, 0
                    FROM dbo.UserName
                    Where NOT EXISTS( SELECT 1 FROM dbo.UserName UN WHERE UN.UserName = @0)";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", userName);
                        _cmd.Parameters.AddWithValue("@1", email);
                        _cmd.Parameters.AddWithValue("@2", password);
                        _cmd.Parameters.AddWithValue("@3", CreatedBy);
                        result = _cmd.ExecuteNonQuery();

                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done save" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data already exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }

        [HttpPost, Route("SaveUserNameDetail")]
        public ResultDynamic SaveUserNameDetail([BindRequired] int userId, [BindRequired] string fullName, int phone, string CreatedBy)
        {

            int result = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    INSERT INTO dbo.DetailUserName (UserId, FullName, Phone, CreatedDate, CreatedBy, RowStatus)
                    SELECT @0, @1, @2, GETDATE(), @3, 0
                    FROM dbo.DetailUserName
                    Where NOT EXISTS( SELECT 1 FROM dbo.DetailUserName DUM WHERE DUM.UserId = @0)
                    AND EXISTS (SELECT 1 FROM dbo.UserName UN WHERE UN.Id = @0)";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", userId);
                        _cmd.Parameters.AddWithValue("@1", fullName);
                        _cmd.Parameters.AddWithValue("@2", phone);
                        _cmd.Parameters.AddWithValue("@3", CreatedBy);
                        result = _cmd.ExecuteNonQuery();

                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done save" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data already exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }

        [HttpPost, Route("UpdateUserNameDetail")]
        public ResultDynamic UpdateUserNameDetail([BindRequired] int userId, [BindRequired] string fullName, int phone, string CreatedBy)
        {

            int result = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    UPDATE dbo.DetailUserName
                    SET FullName = @1,Phone = @2, UpdatedBy = @3, UpdatedDate = GETDATE()
                    Where UserId = @0";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", userId);
                        _cmd.Parameters.AddWithValue("@1", fullName);
                        _cmd.Parameters.AddWithValue("@2", phone);
                        _cmd.Parameters.AddWithValue("@3", CreatedBy);
                        result = _cmd.ExecuteNonQuery();

                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done update" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data not exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }

        [HttpPost, Route("DeleteUserName")]
        public ResultDynamic DeleteUserName([BindRequired] string userName, string CreatedBy)
        {

            int result = 0;
            int userId = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(connectionString))
                {
                    _con.Open();
                    string queryStatement = @";
                    UPDATE INTO dbo.UserName
                    SET RowStatus = 1, UpdatedBy = @1, UpdatedDate = GETDATE()
                    Where UserName = @0";

                    string queryStatement2 = @";
                    SELECT top 1 Id FROM dbo.UserName
                    Where UserName = @0";

                    string queryStatement3 = @";
                    UPDATE INTO dbo.DetailUserName
                    SET RowStatus = 1, UpdatedBy = @1, UpdatedDate = GETDATE()
                    Where UserId = @0";

                    using (SqlCommand _cmd = new SqlCommand(queryStatement, _con))
                    {
                        _cmd.Parameters.AddWithValue("@0", userName);
                        _cmd.Parameters.AddWithValue("@1", CreatedBy);
                        result = _cmd.ExecuteNonQuery();
                    }
                    _con.Close();
                    _con.Open();
                    using (SqlCommand _cmd2 = new SqlCommand(queryStatement2, _con))
                    {

                        _cmd2.Parameters.AddWithValue("@0", userName);
                        SqlDataReader rdr = _cmd2.ExecuteReader();

                        while (rdr.Read())
                        {
                            userId = int.Parse(rdr["Id"].ToString());
                        }
                    }
                    _con.Close();
                    _con.Open();
                    using (SqlCommand _cmd3 = new SqlCommand(queryStatement3, _con))
                    {
                        _cmd3.Parameters.AddWithValue("@0", userId);
                        _cmd3.Parameters.AddWithValue("@1", CreatedBy);
                        result = _cmd3.ExecuteNonQuery();
                    }
                    _con.Close();
                }
                if (result > 0)
                    return new ResultDynamic { status = true, data = result, message = "Data done delete" };
                else
                    return new ResultDynamic { status = false, data = result, message = "Data not exists" };
            }
            catch (Exception e)
            {
                return new ResultDynamic { status = false, errorMessage = e.Message.ToString(), message = API_MESSAGE_ERROR };
            }
        }
    }
}
