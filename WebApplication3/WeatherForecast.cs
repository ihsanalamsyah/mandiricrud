namespace WebApplication3
{
    public class WeatherForecast
    {
        public DateOnly Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string? Summary { get; set; }
    }

    public class Product
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string DescriptionProduct { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; } = null;
        public string? UpdatedBy { get; set; } = null;
    }

    public class Username
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }   
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; } = null;
        public string? UpdatedBy { get; set; } = null;
    }

    public class DetailProduct
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductDescription { get; set; }
        public int Quantity { get; set; } = 0;
        public int Price { get; set; } = 0;
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; } = null;
        public string? UpdatedBy { get; set; } = null;
    }

    public class DetailUserName
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public int Phone { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; } = null;
        public string? UpdatedBy { get; set; } = null;
    }

    public class ProductUserName
    {
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public int Price { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
    }
    public class ResultDynamic
    {
        public bool status { get; set; }
        public string message { get; set; }
        public dynamic data { get; set; }
        public string errorMessage { get; set; }
    }
}
